package coms.client;

import com.sun.istack.internal.Nullable;
import coms.client.common.AbstractEditDialog;
import coms.client.service.StoreDictService;
import coms.client.valueobject.EGoods;
import coms.client.valueobject.EGoodsBarcodes;
import coms.client.valueobject.EGoodsStores;
import coms.client.valueobject.EStore;
import coms.client.view.tablemodel.BarcodeTableModel;
import coms.client.view.tablemodel.StoreTableModel;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GoodsEditDialog extends AbstractEditDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeField;
    private JTextField nameField;
    private JTable barcodesTable;
    private JTable storesTable;
    private JScrollPane barcodeScroll;
    private JScrollPane storeScroll;
    private EGoods goods;

    private JPopupMenu barcodePopUp = new JPopupMenu();
    private JPopupMenu storePopUp = new JPopupMenu();

    private EGoodsBarcodes selectedBarcode;
    private EGoodsStores selectedStore;

    public GoodsEditDialog(@Nullable EGoods goods) {
        if (goods != null) {
            this.goods = goods;
            this.setTitle("Редактирование товара");
        } else {
            this.goods = new EGoods();
            this.goods.setGoodsStoresByN(new ArrayList<>(0));
            this.goods.setGoodsBarcodesByN(new ArrayList<>(0));
            this.setTitle("Новый товар");
        }
        barcodesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        storesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        barcodesTable.setModel(new BarcodeTableModel());
        storesTable.setModel(new StoreTableModel());
        JMenuItem item = new JMenuItem("Добавить");
        item.addActionListener(e -> addBarcode());
        barcodePopUp.add(item);
        item = new JMenuItem("Удалить");
        item.addActionListener(e -> removeBarcode());
        barcodePopUp.add(item);
        item = new JMenuItem("Добавить");
        item.addActionListener(e -> addStore());
        storePopUp.add(item);
        item = new JMenuItem("Удалить");
        item.addActionListener(e -> removeStore());
        storePopUp.add(item);
        barcodesTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    barcodesTable.setRowSelectionInterval(barcodesTable.rowAtPoint(e.getPoint()), barcodesTable.rowAtPoint(e.getPoint()));
                    int index = barcodesTable.convertRowIndexToModel(barcodesTable.getSelectedRow());
                    if (index != -1) {
                        BarcodeTableModel model = (BarcodeTableModel) barcodesTable.getModel();
                        selectedBarcode = model.getBarcodeAt(index);
                        barcodePopUp.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
        storesTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    storesTable.setRowSelectionInterval(storesTable.rowAtPoint(e.getPoint()), storesTable.rowAtPoint(e.getPoint()));
                    int index = storesTable.convertRowIndexToModel(storesTable.getSelectedRow());
                    if (index != -1) {
                        StoreTableModel model = (StoreTableModel) storesTable.getModel();
                        selectedStore = new EGoodsStores();
                        EStore store = model.getStoreAt(index);
                        if (GoodsEditDialog.this.goods != null) {
                            if (GoodsEditDialog.this.goods.getGoodsStoresByN() != null) {
                                for (EGoodsStores link : GoodsEditDialog.this.goods.getGoodsStoresByN())
                                    if (link.getStoresByStoresN().equals(store)) {
                                        selectedStore = link;
                                        break;
                                    }
                                storePopUp.show(e.getComponent(), e.getX(), e.getY());
                            }
                        }
                    }
                }
            }
        });
        barcodeScroll.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (barcodesTable.getRowCount() == 0) {
                        selectedBarcode = new EGoodsBarcodes();
                        selectedBarcode.setGoodsByGoodsN(GoodsEditDialog.this.goods);
                        barcodePopUp.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
        storeScroll.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (storesTable.getRowCount() == 0) {
                        selectedStore = new EGoodsStores();
                        selectedStore.setGoodsByGoodsN(GoodsEditDialog.this.goods);
                        selectedStore.setStoresByStoresN(new EStore());
                        storePopUp.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
        setData(this.goods);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        getData((EGoods) data);
        callbackOK();
        dispose();
    }

    private void onCancel() {
        callbackCansel();
        dispose();
    }

    public void setData(EGoods data) {
        codeField.setText(data.getCode());
        nameField.setText(data.getName());
        ((BarcodeTableModel) barcodesTable.getModel()).setBarcodes((List<EGoodsBarcodes>) data.getGoodsBarcodesByN());
        ((StoreTableModel) storesTable.getModel()).setStores(data.getGoodsStoresByN().stream().map(EGoodsStores::getStoresByStoresN).collect(Collectors.toList()));
        this.data = data;
    }

    public void getData(EGoods data) {
        data.setCode(codeField.getText());
        data.setName(nameField.getText());
        //TODO что добавлять?
    }

    private void addStore() {
        List<EStore> storesList = new ArrayList<>(0);
        storesList.addAll(new StoreDictService().getStoresList());
        storesList.removeIf(store -> goods.getGoodsStoresByN().stream().map(EGoodsStores::getStoresByStoresN).collect(Collectors.toList()).contains(store));
        if (storesList.size() > 0) {
            Object[] variant = storesList.toArray();
            EStore result = (EStore) JOptionPane.showInputDialog(null, "Выберете склад:", "Новая связь", JOptionPane.PLAIN_MESSAGE, null, variant, variant[0]);
            if (result != null) {
                EGoodsStores link = new EGoodsStores();
                link.setGoodsByGoodsN(goods);
                link.setStoresByStoresN(result);
                goods.getGoodsStoresByN().add(link);
                List<EStore> stores = goods.getGoodsStoresByN().stream().map(EGoodsStores::getStoresByStoresN).collect(Collectors.toList());
                ((StoreTableModel) storesTable.getModel()).setStores(stores);
                ((StoreTableModel) storesTable.getModel()).fireTableDataChanged();
            }
        }
    }

    private void removeStore() {
        if (selectedStore != null) {
            goods.getGoodsStoresByN().remove(selectedStore);
            List<EStore> stores = goods.getGoodsStoresByN().stream().map(EGoodsStores::getStoresByStoresN).collect(Collectors.toList());
            ((StoreTableModel) storesTable.getModel()).setStores(stores);
            ((StoreTableModel) storesTable.getModel()).fireTableDataChanged();
        }
    }

    private void addBarcode() {
        String code = (String) JOptionPane.showInputDialog(null, "Штрихкод:", "Новый штрихкод", JOptionPane.PLAIN_MESSAGE, null, null, null);
        if (code != null) {
            EGoodsBarcodes barcode = new EGoodsBarcodes();
            barcode.setBarcode(code);
            barcode.setGoodsByGoodsN(goods);
            goods.getGoodsBarcodesByN().add(barcode);
            ((BarcodeTableModel) barcodesTable.getModel()).fireTableDataChanged();
        }
    }

    private void removeBarcode() {
        if (selectedBarcode != null) {
            goods.getGoodsBarcodesByN().remove(selectedBarcode);
            ((BarcodeTableModel) barcodesTable.getModel()).fireTableDataChanged();
        }
    }
}
