package coms.client.service;

import coms.client.common.CommonProvider;
import coms.client.valueobject.EGoods;
import coms.client.valueobject.EGoodsStores;
import coms.client.valueobject.EStore;
import coms.exchange.*;

import java.util.List;
import java.util.function.Consumer;

public class GoodsDictService {
    @SuppressWarnings("unchecked")
    public List<EGoods> getGoodsList() {
        CommonProvider<Object, EGoods> provider = new CommonProvider<>("/goods/list", EGoods.class);
        List<EGoods> goodsLIst = provider.get();
        return provider.get();
    }

    public void addGoods(EGoods goods) {
        CommonProvider<JGoodsAdd, JGoodsAddResponse> provider = new CommonProvider<>("/goods/add", JGoodsAddResponse.class);
        JGoodsAdd request = new JGoodsAdd();
        request.setGoods(goods);
        provider.post(request);
    }

    public void removeGoods(EGoods goods) {
        CommonProvider<JGoodsRemove, JGoodsRemoveResponse> provider = new CommonProvider<>("/goods/remove", JGoodsRemoveResponse.class);
        JGoodsRemove request = new JGoodsRemove();
        request.setGoodsN(goods.getN());
        provider.post(request);
    }

    public void editGoods(EGoods goods) {
        CommonProvider<JGoodsEdit, JGoodsEditResponse> provider = new CommonProvider<>("/goods/edit", JGoodsEditResponse.class);
        JGoodsEdit request = new JGoodsEdit();
        request.setGoods(goods);
        provider.post(request);
    }

    public void addStoreLink(EGoods goods, EStore store) {
        CommonProvider<JGoodsStoreLinkAdd, JGoodsStoreLinkAddResponse> provider = new CommonProvider<>("/goods/addlink", JGoodsStoreLinkAddResponse.class);
        JGoodsStoreLinkAdd request = new JGoodsStoreLinkAdd();
        request.setGoods(goods);
        request.setStore(store);
        provider.post(request);
    }

    public void removeStoreLink(EGoods goods, EStore store) {
        CommonProvider<JGoodsStoreLinkRemove, JGoodsStoreLinkRemoveResponse> provider = new CommonProvider<>("/goods/removelinks", JGoodsStoreLinkRemoveResponse.class);
        JGoodsStoreLinkRemove request = new JGoodsStoreLinkRemove();
        request.setGoods(goods);
        request.setStore(store);
        provider.post(request);
    }
}
