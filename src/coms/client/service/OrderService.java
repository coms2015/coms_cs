package coms.client.service;

import coms.client.common.CommonProvider;
import coms.client.valueobject.EOrder;
import coms.exchange.*;

import java.util.ArrayList;
import java.util.List;

public class OrderService {
    public List<EOrder> getOrders() {
        CommonProvider<Object, EOrder> provider = new CommonProvider<>("/order/list", EOrder.class);
        List<EOrder> orders = provider.get();
        for (EOrder order : orders) {
            if (order.getOrdersGoodsesByN() == null)
                order.setOrdersGoodsesByN(new ArrayList<>(0));
            order.getOrdersGoodsesByN().forEach(g -> g.setOrdersByOrdersN(order));
        }
        return orders;
    }

    public void addOrder(EOrder order) {
        CommonProvider<JOrderAdd, JOrderAddResponse> provider = new CommonProvider<>("/order/add", JOrderAddResponse.class);
        JOrderAdd request = new JOrderAdd();
        request.setOrder(order);
        provider.post(request);
    }

    public void removeOrder(EOrder order) {
        CommonProvider<JOrderRemove,JOrderRemoveResponse> provider = new CommonProvider<>("/order/remove", JOrderRemoveResponse.class);
        JOrderRemove request = new JOrderRemove();
        request.setOrder(order);
        provider.post(request);
    }

    public void reserveOrder(EOrder order) {
        CommonProvider<JOrderReserve, JOrderReserveResponse> provider = new CommonProvider<>("/order/reserve", JOrderReserveResponse.class);
        JOrderReserve request = new JOrderReserve();
        request.setOrder(order);
        provider.post(request);
    }

    public void outOrder(EOrder order) {
        CommonProvider<JOrderOut, JOrderOutResponse> provider = new CommonProvider<>("/order/out", JOrderOutResponse.class);
        JOrderOut request = new JOrderOut();
        request.setOrder(order);
        provider.post(request);
    }
//
//    public void editOrder(EOrder order) {
//        CommonProvider<JOrderEdit, JOrderEditResponse> provider = new CommonProvider<>("/order/edit", JOrderEditResponse.class);
//        JOrderEdit request = new JOrderEdit();
//        request.setOrder(order);
//        provider.post(request);
//    }
}
