package coms.client.service;

import coms.client.common.CommonProvider;
import coms.client.valueobject.EReceipt;
import coms.exchange.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ReceiptService {
    public List<EReceipt> getReceipts() {
        CommonProvider<Object, EReceipt> provider = new CommonProvider<>("/receipt/list", EReceipt.class);
        List<EReceipt> receipts = provider.get();
        for (EReceipt receipt : receipts) {
            if (receipt.getReceiptsGoodsesByN() == null)
                receipt.setReceiptsGoodsesByN(new ArrayList<>(0));
            receipt.getReceiptsGoodsesByN().forEach(eReceiptsGoods -> eReceiptsGoods.setReceiptsByReceiptsN(receipt));
        }
        return receipts;
    }

    public void addReceipt(EReceipt receipt) {
        CommonProvider<JReceiptAdd, JReceiptAddResponse> provider = new CommonProvider<>("/receipt/add", JReceiptAddResponse.class);
        JReceiptAdd request = new JReceiptAdd();
        request.setReceipt(receipt);
        provider.post(request);
    }

    public void removeReceipt(EReceipt receipt) {
        CommonProvider<JReceiptRemove, JReceiptRemoveResponse> provider = new CommonProvider<>("/receipt/remove", JReceiptRemoveResponse.class);
        JReceiptRemove request = new JReceiptRemove();
        request.setReceipt(receipt);
        provider.post(request);
    }

    public void editReceipt(EReceipt receipt) {
        CommonProvider<JReceiptEdit, JReceiptEditResponse> provider = new CommonProvider<>("/receipt/edit", JReceiptEditResponse.class);
        JReceiptEdit request = new JReceiptEdit();
        request.setReceipt(receipt);
        provider.post(request);
    }

    public void toStock(EReceipt receipt) {
        CommonProvider<JReceiptIn, JReceiptInResponse> provider = new CommonProvider<>("/receipt/in", JReceiptInResponse.class);
        JReceiptIn request = new JReceiptIn();
        request.setReceipt(receipt);
        provider.post(request);
    }
}
