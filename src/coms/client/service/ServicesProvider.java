package coms.client.service;

public final class ServicesProvider {
    // start singleton methods
    private static volatile ServicesProvider instance = null;

    private ServicesProvider(){}

    public static ServicesProvider getInstance() {
        ServicesProvider local = instance;
        if (local == null) {
            synchronized (ServicesProvider.class) {
                local = instance;
                if (local == null) {
                    local = instance = new ServicesProvider();
                }
            }
        }
        return local;
    }
    // end singleton methods

    private volatile AuthService authService = null;
    private volatile StoreDictService storeDictService = null;
    private volatile GoodsDictService goodsDictService = null;
    private volatile ReceiptService receiptService = null;
    private volatile OrderService orderService = null;

    public synchronized AuthService getAuthService() {
        if (authService == null)
            return authService = new AuthService();
        return authService;
    }

    public synchronized StoreDictService getStoreDictService() {
        if (storeDictService == null)
            return storeDictService = new StoreDictService();
        return storeDictService;
    }

    public synchronized GoodsDictService getGoodsDictService() {
        if (goodsDictService == null)
            return goodsDictService = new GoodsDictService();
        return goodsDictService;
    }

    public synchronized ReceiptService getReceiptService() {
        if (receiptService == null)
            return receiptService = new ReceiptService();
        return receiptService;
    }

    public synchronized OrderService getOrderService() {
        if (orderService == null)
            return orderService = new OrderService();
        return orderService;
    }
}
