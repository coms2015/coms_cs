package coms.client.service;

import coms.client.SHA1Hasher;
import coms.client.exception.*;
import coms.client.common.*;
import coms.exchange.JAuth;
import coms.exchange.JAuthResponse;
import coms.exchange.JLogout;
import coms.exchange.JLogoutResponse;

import javax.ws.rs.core.NewCookie;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AuthService {
    private boolean isAuthed = false;
    private Set<String> modules = new HashSet<>();
    private String sessionId = null;

    public boolean login(String login, String password) throws PrivilegesException, ServerException {
        CommonProvider<JAuth, JAuthResponse> provider = new CommonProvider<>("/auth", JAuthResponse.class);
        isAuthed = false;
        JAuth jAuth = new JAuth();
        jAuth.setLogin(login);
        jAuth.setHash(SHA1Hasher.SHA1(password));
        try {
            JAuthResponse response = provider.post(jAuth);
            isAuthed = true;
            modules.addAll(Arrays.asList(response.getModules()));
        } catch (AuthException e) {
            return false;
            }
        return true;
    }

    public void logout() {
        isAuthed = false;
        modules.clear();
        CommonProvider<JLogout, JLogoutResponse> provider = new CommonProvider<>("/logout", JLogoutResponse.class);
        JLogout jLogout = new JLogout();
        provider.post(jLogout);
        sessionId = null;
    }

    public boolean checkModule(String module) {
        return modules.contains(module);
    }

    public boolean isAuthed() {
        return isAuthed;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
