package coms.client.service;

import coms.client.common.*;
import coms.client.valueobject.EStore;
import coms.exchange.*;

import java.util.ArrayList;
import java.util.List;

public class StoreDictService {
    @SuppressWarnings("unchecked")
    public List<EStore> getStoresList() {
        CommonProvider<Object, EStore> provider = new CommonProvider<>("/store/list", EStore.class);
        return provider.get();
    }

    public void addStore(EStore store) {
        CommonProvider<JStoreAdd, JStoreAddResponse> provider = new CommonProvider<>("/store/add", JStoreAddResponse.class);
        JStoreAdd request = new JStoreAdd();
        request.setStore(store);
        provider.post(request);
    }

    public void removeStore(EStore store) {
        CommonProvider<JStoreRemove, JStoreRemoveResponse> provider = new CommonProvider<>("/store/remove", JStoreRemoveResponse.class);
        JStoreRemove request = new JStoreRemove();
        request.setStoreN(store.getN());
        provider.post(request);
    }

    public void editStore(EStore store) {
        CommonProvider<JStoreEdit, JStoreEditResponse> provider = new CommonProvider<>("/store/edit", JStoreEditResponse.class);
        JStoreEdit request = new JStoreEdit();
        request.setStore(store);
        provider.post(request);
    }
}
