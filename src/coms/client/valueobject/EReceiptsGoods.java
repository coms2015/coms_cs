package coms.client.valueobject;

import org.codehaus.jackson.annotate.JsonIgnore;

public class EReceiptsGoods implements IValueObject {
    private Integer amount;
    private Integer n;
    private EGoods goodsByGoodsN;
    private EReceipt receiptsByReceiptsN;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EReceiptsGoods that = (EReceiptsGoods) o;

        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (n != null ? !n.equals(that.n) : that.n != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (n != null ? n.hashCode() : 0);
        return result;
    }

    public EGoods getGoodsByGoodsN() {
        return goodsByGoodsN;
    }

    public void setGoodsByGoodsN(EGoods goodsByGoodsN) {
        this.goodsByGoodsN = goodsByGoodsN;
    }

    @JsonIgnore
    public EReceipt getReceiptsByReceiptsN() {
        return receiptsByReceiptsN;
    }

    public void setReceiptsByReceiptsN(EReceipt receiptsByReceiptsN) {
        this.receiptsByReceiptsN = receiptsByReceiptsN;
    }
}
