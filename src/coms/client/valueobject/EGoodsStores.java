package coms.client.valueobject;

import org.codehaus.jackson.annotate.JsonIgnore;

public class EGoodsStores implements IValueObject {
    private Integer n;
    private EGoods goodsByGoodsN;
    private EStore storesByStoresN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EGoodsStores that = (EGoodsStores) o;

        if (n != null ? !n.equals(that.n) : that.n != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return n != null ? n.hashCode() : 0;
    }

    @JsonIgnore
    public EGoods getGoodsByGoodsN() {
        return goodsByGoodsN;
    }

    public void setGoodsByGoodsN(EGoods goodsByGoodsN) {
        this.goodsByGoodsN = goodsByGoodsN;
    }

    public EStore getStoresByStoresN() {
        return storesByStoresN;
    }

    public void setStoresByStoresN(EStore storesByStoresN) {
        this.storesByStoresN = storesByStoresN;
    }
}
