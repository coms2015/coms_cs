package coms.client.valueobject;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Collection;

public class EStore implements IValueObject {
    private Integer n;
    private String name;
    private String address;
//    private Collection<EGoodsStores> goodsStoresByN;
//    private Collection<EOrder> ordersesByN;
//    private Collection<EReceipt> receiptsesByN;
//    private Collection<EStock> stocksByN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EStore eStore = (EStore) o;

        return n == eStore.n && !(name != null ? !name.equals(eStore.name) : eStore.name != null) && !(address != null ? !address.equals(eStore.address) : eStore.address != null);

    }

    @Override
    public int hashCode() {
        int result = n;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name + " (" + address + ")";
    }

    @JsonIgnore
    //@Override
    public Object[] getTableData() {
        Object[] data = new Object[3];
        data[0] = name;
        data[1] = address;
        data[2] = n;
        return data;
    }
}
