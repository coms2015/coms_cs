package coms.client.valueobject;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Collection;

public class EUser implements IValueObject {
    private Integer n;
    private String login;
    private String mail;
    private String password;
    private Integer rolesN;
    private ERole rolesByRolesN;
    private Collection<EOrder> ordersesByN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRolesN() {
        return rolesN;
    }

    public void setRolesN(Integer rolesN) {
        this.rolesN = rolesN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EUser user = (EUser) o;

        return !(n != null ? !n.equals(user.n) : user.n != null) && !(login != null ? !login.equals(user.login) : user.login != null) && !(mail != null ? !mail.equals(user.mail) : user.mail != null) && !(password != null ? !password.equals(user.password) : user.password != null) && !(rolesN != null ? !rolesN.equals(user.rolesN) : user.rolesN != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (mail != null ? mail.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (rolesN != null ? rolesN.hashCode() : 0);
        return result;
    }

    public ERole getRolesByRolesN() {
        return rolesByRolesN;
    }

    public void setRolesByRolesN(ERole rolesByRolesN) {
        this.rolesByRolesN = rolesByRolesN;
    }

    @JsonIgnore
    public Collection<EOrder> getOrdersesByN() {
        return ordersesByN;
    }

    public void setOrdersesByN(Collection<EOrder> ordersesByN) {
        this.ordersesByN = ordersesByN;
    }
}
