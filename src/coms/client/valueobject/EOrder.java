package coms.client.valueobject;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

public class EOrder implements IValueObject {
    private Integer n;
    private String code;
    private Timestamp createdAt;
    private Date executeAt;
    private Integer executed;
    private EStore storesByStoresN;
    private EUser usersByUsersN;
    private Collection<EOrdersGoods> ordersGoodsesByN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Date getExecuteAt() {
        return executeAt;
    }

    public void setExecuteAt(Date executeAt) {
        this.executeAt = executeAt;
    }

    public Integer getExecuted() {
        return executed;
    }

    public void setExecuted(Integer executed) {
        this.executed = executed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EOrder eOrder = (EOrder) o;

        if (n != null ? !n.equals(eOrder.n) : eOrder.n != null) return false;
        if (code != null ? !code.equals(eOrder.code) : eOrder.code != null) return false;
        if (createdAt != null ? !createdAt.equals(eOrder.createdAt) : eOrder.createdAt != null) return false;
        if (executeAt != null ? !executeAt.equals(eOrder.executeAt) : eOrder.executeAt != null) return false;
        if (executed != null ? !executed.equals(eOrder.executed) : eOrder.executed != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (executeAt != null ? executeAt.hashCode() : 0);
        result = 31 * result + (executed != null ? executed.hashCode() : 0);
        return result;
    }

    public EStore getStoresByStoresN() {
        return storesByStoresN;
    }

    public void setStoresByStoresN(EStore storesByStoresN) {
        this.storesByStoresN = storesByStoresN;
    }

    public EUser getUsersByUsersN() {
        return usersByUsersN;
    }

    public void setUsersByUsersN(EUser usersByUsersN) {
        this.usersByUsersN = usersByUsersN;
    }

    public Collection<EOrdersGoods> getOrdersGoodsesByN() {
        return ordersGoodsesByN;
    }

    public void setOrdersGoodsesByN(Collection<EOrdersGoods> ordersGoodsesByN) {
        this.ordersGoodsesByN = ordersGoodsesByN;
    }
}
