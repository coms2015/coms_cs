package coms.client.valueobject;

import java.util.Collection;

public class EGoods implements IValueObject {
    private Integer n;
    private String code;
    private String name;
    private Collection<EGoodsBarcodes> goodsBarcodesByN;
    private Collection<EGoodsStores> goodsStoresByN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EGoods eGoods = (EGoods) o;

        return !(n != null ? !n.equals(eGoods.n) : eGoods.n != null) && !(code != null ? !code.equals(eGoods.code) : eGoods.code != null) && !(name != null ? !name.equals(eGoods.name) : eGoods.name != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public Collection<EGoodsBarcodes> getGoodsBarcodesByN() {
        return goodsBarcodesByN;
    }

    public void setGoodsBarcodesByN(Collection<EGoodsBarcodes> goodsBarcodesByN) {
        this.goodsBarcodesByN = goodsBarcodesByN;
    }

    public Collection<EGoodsStores> getGoodsStoresByN() {
        return goodsStoresByN;
    }

    public void setGoodsStoresByN(Collection<EGoodsStores> goodsStoresByN) {
        this.goodsStoresByN = goodsStoresByN;
    }

    @Override
    public String toString() {
        return "[" + getCode() + "] " + getName();
    }
}
