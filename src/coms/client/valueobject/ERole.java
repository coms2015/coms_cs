package coms.client.valueobject;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Collection;

public class ERole implements IValueObject {
    private Integer n;
    private String name;
    private String modules;
    private Collection<EUser> usersesByN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModules() {
        return modules;
    }

    public void setModules(String modules) {
        this.modules = modules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ERole role = (ERole) o;

        return !(n != null ? !n.equals(role.n) : role.n != null) && !(name != null ? !name.equals(role.name) : role.name != null) && !(modules != null ? !modules.equals(role.modules) : role.modules != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (modules != null ? modules.hashCode() : 0);
        return result;
    }

    @JsonIgnore
    public Collection<EUser> getUsersesByN() {
        return usersesByN;
    }

    public void setUsersesByN(Collection<EUser> usersesByN) {
        this.usersesByN = usersesByN;
    }
}
