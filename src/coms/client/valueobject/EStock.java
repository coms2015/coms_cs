package coms.client.valueobject;

import java.math.BigDecimal;

public class EStock implements IValueObject {
    private Integer n;
    private Integer amount;
    private BigDecimal price;
    private Integer inOrder;
    private EGoods goodsByGoodsN;
    private EStore storesByStoresN;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getInOrder() {
        return inOrder;
    }

    public void setInOrder(Integer inOrder) {
        this.inOrder = inOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EStock eStock = (EStock) o;

        if (n != null ? !n.equals(eStock.n) : eStock.n != null) return false;
        if (amount != null ? !amount.equals(eStock.amount) : eStock.amount != null) return false;
        if (price != null ? !price.equals(eStock.price) : eStock.price != null) return false;
        if (inOrder != null ? !inOrder.equals(eStock.inOrder) : eStock.inOrder != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (inOrder != null ? inOrder.hashCode() : 0);
        return result;
    }

    public EGoods getGoodsByGoodsN() {
        return goodsByGoodsN;
    }

    public void setGoodsByGoodsN(EGoods goodsByGoodsN) {
        this.goodsByGoodsN = goodsByGoodsN;
    }

    public EStore getStoresByStoresN() {
        return storesByStoresN;
    }

    public void setStoresByStoresN(EStore storesByStoresN) {
        this.storesByStoresN = storesByStoresN;
    }
}
