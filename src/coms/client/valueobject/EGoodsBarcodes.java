package coms.client.valueobject;

import org.codehaus.jackson.annotate.JsonIgnore;

public class EGoodsBarcodes implements IValueObject {
    private String barcode;
    private Integer n;
    private EGoods goodsByGoodsN;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EGoodsBarcodes that = (EGoodsBarcodes) o;

        if (barcode != null ? !barcode.equals(that.barcode) : that.barcode != null) return false;
        if (n != null ? !n.equals(that.n) : that.n != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = barcode != null ? barcode.hashCode() : 0;
        result = 31 * result + (n != null ? n.hashCode() : 0);
        return result;
    }

    @JsonIgnore
    public EGoods getGoodsByGoodsN() {
        return goodsByGoodsN;
    }

    public void setGoodsByGoodsN(EGoods goodsByGoodsN) {
        this.goodsByGoodsN = goodsByGoodsN;
    }
}
