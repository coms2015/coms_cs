package coms.client;

import coms.client.common.AbstractEditDialog;
import coms.client.service.ServicesProvider;
import coms.client.valueobject.EReceipt;
import coms.client.valueobject.EStore;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.event.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

public class ReceiptEditDialog extends AbstractEditDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeField;
    private JComboBox storeBox;
    private JXDatePicker arrivalDatePicker;

    @SuppressWarnings("unchecked")
    public ReceiptEditDialog() {
        this.data = new EReceipt();
        this.setTitle("Новый приход");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        Object[] stores = ServicesProvider.getInstance().getStoreDictService().getStoresList().toArray();
        storeBox.setModel(new DefaultComboBoxModel<>(stores));

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        EReceipt receipt = (EReceipt) data;
        receipt.setCode(codeField.getText());
        receipt.setStoresByStoresN((EStore) storeBox.getSelectedItem());
        receipt.setArrivalAt(new Date(arrivalDatePicker.getDate().getTime()));
        receipt.setCreatedAt(new Timestamp(Calendar.getInstance().getTime().getTime()));
        receipt.setArrived(0);
        callbackOK();
        dispose();
    }

    private void onCancel() {
        callbackCansel();
        dispose();
    }
}
