package coms.client.exception;

public final class PrivilegesException extends WorkflowException {
    public PrivilegesException() {
        super();
    }

    @Override
    public String getAppMessage() {
        return "Недостаточно привелегий";
    }
}
