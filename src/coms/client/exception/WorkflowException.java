package coms.client.exception;

public abstract class WorkflowException extends RuntimeException {
    public WorkflowException() {
        super();
    }

    public abstract String getAppMessage();
}
