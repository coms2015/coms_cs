package coms.client.exception;

public class ServerException extends CriticalException {
    public ServerException() {
        super();
    }

    public ServerException(String message) {
        super(message);
    }
}
