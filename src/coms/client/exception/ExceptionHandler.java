package coms.client.exception;

import org.apache.logging.log4j.*;

import javax.swing.*;
import java.awt.*;

public final class ExceptionHandler {

    private static final Logger logger = LogManager.getLogger(ExceptionHandler.class);

    public static void handleException(Thread thread, Throwable e) {

        if (e instanceof CriticalException) {
            logger.error(String.format("CriticalException in thread %s: ", thread), e);
            JOptionPane.showMessageDialog(null, "Критическая ошибка: " + e.getMessage() + "\n(" + e.toString() + ")", "COMS", JOptionPane.ERROR_MESSAGE);
            for (Frame frame : Frame.getFrames()) {
                frame.dispose();
            }
        } else if (e instanceof WorkflowException) {
            logger.warn(String.format("WorkflowException in thread %s: ", thread), e);
            JOptionPane.showMessageDialog(null, ((WorkflowException) e).getAppMessage() + "\n(" + e.toString() + ")", "COMS", JOptionPane.WARNING_MESSAGE);
        } else {
            logger.error(String.format("Exception in thread %s: ", thread), e);
            //TODO только на время разработки
            if (JOptionPane.showConfirmDialog(null, e.getMessage() + "\nПродолжить выполнение программы?", "COMS", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE) == JOptionPane.NO_OPTION) {
                for (Frame frame : Frame.getFrames()) {
                    frame.dispose();
                }
            }
        }
    }
}
