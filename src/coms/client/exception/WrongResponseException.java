package coms.client.exception;

public final class WrongResponseException extends CriticalException {
    public WrongResponseException() {
        super();
    }

    public WrongResponseException(String message) {
        super(message);
    }
}
