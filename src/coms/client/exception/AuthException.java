package coms.client.exception;

public final class AuthException extends WorkflowException {
    public AuthException() {
        super();
    }

    @Override
    public String getAppMessage() {
        return "Вы не авторизованы";
    }
}