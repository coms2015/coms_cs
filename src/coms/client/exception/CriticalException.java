package coms.client.exception;

public abstract class CriticalException extends RuntimeException {
    public CriticalException() {
        super();
    }

    public CriticalException(String message) {
        super(message);
    }
}
