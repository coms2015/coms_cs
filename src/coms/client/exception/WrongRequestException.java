package coms.client.exception;

public final class WrongRequestException extends CriticalException {
    public WrongRequestException() {
        super();
    }
}
