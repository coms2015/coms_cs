package coms.client;

import coms.client.exception.ExceptionHandler;
import coms.client.view.*;
import org.apache.logging.log4j.core.config.Order;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ComsForm {
    private JTabbedPane commonTabs;
    private JPanel mainPanel;
    private JPanel storesTab;
    private JPanel goodsTab;
    private JTable storesStoresTable;
    private JButton storesAddStoreButton;
    private JButton storesRefreshButton;
    private JTable goodsGoodsTable;
    private JButton goodsRefreshButton;
    private JButton goodsAddGoodsButton;
    private JPanel receiptsTab;
    private JPanel ordersTab;
    private JButton recieptsAddReceiptButton;
    private JButton receiptsRefreshButton;
    private JButton ordersRefreshButton;
    private JPanel accountTab;
    private JXTreeTable receiptsTable;
    private JXTreeTable ordersTable;

    private static JFrame mainFrame;

    private static User user;
    private Stores stores;
    private Goods goods;
    private Receipts receipts;
    private Orders orders;

    public ComsForm() {
        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler::handleException);
        storesStoresTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        goodsGoodsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        stores = new Stores(storesStoresTable);
        goods = new Goods(goodsGoodsTable);
        receipts = new Receipts(receiptsTable);
        orders = new Orders(ordersTable);
        //TODO временно
        storesRefreshButton.addActionListener(e -> stores.refreshTable());
        storesAddStoreButton.addActionListener(e -> stores.newStore());
        goodsRefreshButton.addActionListener(e -> goods.refreshTable());
        goodsAddGoodsButton.addActionListener(e -> goods.newGoods());
        receiptsRefreshButton.addActionListener(e -> receipts.refreshTable());
        recieptsAddReceiptButton.addActionListener(e -> receipts.newReceipt());
        ordersRefreshButton.addActionListener(e -> orders.refreshTable());
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(
                UIManager.getSystemLookAndFeelClassName());

        user = new User();

        JFrame frame = new JFrame("СOMS");
        frame.setContentPane(new ComsForm().mainPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                user.logout();
            }
        });
        frame.pack();

        if (!user.showLoginForm())
            return;
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        mainFrame = frame;
    }
}
