package coms.client;

import com.sun.istack.internal.Nullable;
import coms.client.common.AbstractEditDialog;
import coms.client.valueobject.EStore;

import javax.swing.*;
import java.awt.event.*;

public class StoreEditDialog extends AbstractEditDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nameField;
    private JTextArea addressArea;

    public StoreEditDialog(@Nullable EStore store) {
        if (store != null) {
            this.data = store;
            this.setTitle("Редактирование склада");
        } else {
            this.data = new EStore();
            this.setTitle("Новый склад");
        }
        setData((EStore) this.data);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        if (isModified((EStore) data)) {
            getData((EStore) data);
            callbackOK();
        }
        dispose();
    }

    private void onCancel() {
        callbackCansel();
        dispose();
    }

    private void setData(EStore data) {
        nameField.setText(data.getName());
        addressArea.setText(data.getAddress());
    }

    private void getData(EStore data) {
        data.setName(nameField.getText());
        data.setAddress(addressArea.getText());
    }

    public boolean isModified(EStore data) {
        return (nameField.getText() != null ? !nameField.getText().equals(data.getName()) : data.getName() != null) || (addressArea.getText() != null ? !addressArea.getText().equals(data.getAddress()) : data.getAddress() != null);
    }
}
