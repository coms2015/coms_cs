package coms.client.view;

import coms.client.common.AbstractEditDialog;
import coms.client.common.IEditDialogResultListener;
import coms.client.service.OrderService;
import coms.client.service.ServicesProvider;
import coms.client.valueobject.EOrder;
import coms.client.valueobject.EOrdersGoods;
import coms.client.valueobject.IValueObject;
import coms.client.view.tablemodel.OrdersTreeTableModel;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class Orders implements IEditDialogResultListener {
    private OrderService orderService = ServicesProvider.getInstance().getOrderService();
    private JXTreeTable table;
    private OrderPopUpMenu orderPopUpMenu = new OrderPopUpMenu();

    public Orders(JXTreeTable table) {
        this.table = table;
        table.setTreeTableModel(new OrdersTreeTableModel(new ArrayList<>(0)));
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    int row = table.rowAtPoint(e.getPoint());
                    if (row == -1)
                        return;
                    table.setRowSelectionInterval(row, row);
                    int index = table.convertRowIndexToModel(table.getSelectedRow());
                    if (index != -1) {
                        TreePath path = table.getPathForRow(index);
                        Object node = path.getLastPathComponent();
                        if (node instanceof EOrder) {
                            EOrder order = (EOrder) node;
                            orderPopUpMenu.setSelectedOrder(order).show(e.getComponent(), e.getX(), e.getY());
                        }
                    }
                }
            }
        });
    }

    private void retrieveOrders() {
        table.setTreeTableModel(new OrdersTreeTableModel(orderService.getOrders()));
        ((AbstractTableModel) table.getModel()).fireTableDataChanged();
    }

    public void refreshTable() {
        retrieveOrders();
        table.getColumn("Код").setPreferredWidth(20000);
        table.getColumn("Склад/Товар").setPreferredWidth(40000);
        table.getColumn("Кол-во").setPreferredWidth(10000);
        table.getColumn("В резерве").setPreferredWidth(10000);
        table.getColumn("Ожидается выдача").setPreferredWidth(15000);
    }

    @Override
    public void onEditDialogOK(AbstractEditDialog dialog, IValueObject valueObject) {

    }

    @Override
    public void onEditDialogCancel(AbstractEditDialog dialog) {

    }

    private class OrderPopUpMenu extends JPopupMenu {
        private EOrder selectedOrder;
        private JMenuItem reserveItem;
        private JMenuItem outIten;

        public OrderPopUpMenu() {
            super();
            reserveItem = new JMenuItem("В резерв");
            reserveItem.addActionListener(e -> onReserveClick());
            this.add(reserveItem);
            outIten = new JMenuItem("Отгрузить");
            outIten.addActionListener(e -> onOutClick());
            this.add(outIten);
            JMenuItem item = new JMenuItem("Удалить");
            item.addActionListener(e -> onDeleteClick());
            this.add(item);
        }

        public OrderPopUpMenu setSelectedOrder(EOrder order) {
            selectedOrder = order;
            if (selectedOrder.getExecuted() == 0) {
                reserveItem.setEnabled(true);
                outIten.setEnabled(false);
            } else {
                reserveItem.setEnabled(false);
                reserveItem.setEnabled(true);
            }
            return this;
        }

        private void onReserveClick() {
            if (JOptionPane.showConfirmDialog(null, "Подтвердите резерв заказа", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                orderService.reserveOrder(selectedOrder);
                refreshTable();
            }
        }

        private void onOutClick() {
            if (JOptionPane.showConfirmDialog(null, "Подтвердите отгрузку заказа", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                orderService.outOrder(selectedOrder);
                refreshTable();
            }
        }

        private void onDeleteClick() {
            if (JOptionPane.showConfirmDialog(null, "Вы уверены, что хотите удалить документ?", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                orderService.removeOrder(selectedOrder);
                refreshTable();
            }
        }
    }
}
