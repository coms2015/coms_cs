package coms.client.view;

import coms.client.service.AuthService;
import coms.client.service.ServicesProvider;
import org.jdesktop.swingx.JXLoginPane;
import org.jdesktop.swingx.auth.LoginService;

import javax.swing.*;

public class User {
    private AuthService authService = ServicesProvider.getInstance().getAuthService();

    public boolean showLoginForm() {
        JXLoginPane.JXLoginDialog dialog = new JXLoginPane.JXLoginDialog((JFrame) null, new JXLoginPane(new ComsLoginService()));
        dialog.setAutoRequestFocus(true);
        dialog.setVisible(true);
        return dialog.getStatus() == JXLoginPane.Status.SUCCEEDED;
    }

    public void logout() {
        authService.logout();
    }

    public final class ComsLoginService extends LoginService {
        @Override
        public boolean authenticate(String login, char[] passChars, String s1) throws Exception {
            String password = new String(passChars);
            return authService.login(login, password);
        }
    }
}
