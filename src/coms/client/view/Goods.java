package coms.client.view;

import coms.client.GoodsEditDialog;
import coms.client.common.AbstractEditDialog;
import coms.client.common.IEditDialogResultListener;
import coms.client.service.GoodsDictService;
import coms.client.service.ServicesProvider;
import coms.client.valueobject.EGoods;
import coms.client.valueobject.IValueObject;
import coms.client.view.tablemodel.GoodsTableModel;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Goods implements IEditDialogResultListener {
    private GoodsDictService goodsDictService = ServicesProvider.getInstance().getGoodsDictService();
    private JTable table;
    private GoodsTablePopUp popUp = new GoodsTablePopUp();

    public Goods(JTable table) {
        this.table = table;
        table.setModel(new GoodsTableModel());
        table.setAutoCreateRowSorter(true);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    table.setRowSelectionInterval(table.rowAtPoint(e.getPoint()), table.rowAtPoint(e.getPoint()));
                    int index = table.convertRowIndexToModel(table.getSelectedRow());
                    if (index != -1) {
                        GoodsTableModel model = (GoodsTableModel) table.getModel();
                        EGoods goods = model.getGoodsAt(index);
                        popUp.setSelectedGoods(goods).show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
    }

    public void newGoods() {
        GoodsEditDialog editDialog = new GoodsEditDialog(null);
        editDialog.addDialogResultListener(this);
        editDialog.showDialog();
    }

    private void retrieveStores() {
        GoodsTableModel model = (GoodsTableModel) table.getModel();
        model.setGoods(goodsDictService.getGoodsList());
        model.fireTableDataChanged();
    }

    public void refreshTable() {
        retrieveStores();
        table.getColumn("Код").setPreferredWidth(3000);
        table.getColumn("Наименование").setPreferredWidth(7000);
    }

    @Override
    public void onEditDialogOK(AbstractEditDialog dialog, IValueObject valueObject) {
        EGoods goods = (EGoods) valueObject;
        if (goods.getN() != null)
            goodsDictService.editGoods(goods);
        else
            goodsDictService.addGoods(goods);
        //TODO обновление только скроки, если операция прошла
        refreshTable();
    }

    @Override
    public void onEditDialogCancel(AbstractEditDialog dialog) {

    }

    private class GoodsTablePopUp extends JPopupMenu {
        private EGoods selectedGoods;

        public GoodsTablePopUp() {
            super();
            JMenuItem item = new JMenuItem("Изменить...");
            item.addActionListener(e -> onEditClick());
            add(item);
            item = new JMenuItem("Удалить");
            item.addActionListener(e -> onRemoveClick());
            add(item);
        }

        public GoodsTablePopUp setSelectedGoods(EGoods selectedGoods) {
            this.selectedGoods = selectedGoods;
            return this;
        }

        private void onEditClick() {
            GoodsEditDialog editDialog = new GoodsEditDialog(selectedGoods);
            editDialog.addDialogResultListener(Goods.this);
            editDialog.showDialog();
        }

        private void onRemoveClick() {
            if (JOptionPane.showConfirmDialog(null, "Вы уверены?\nУдаление товара приведет к удалению всех его из всех документов и стока. Эта операция не обратима", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                goodsDictService.removeGoods(selectedGoods);
                refreshTable();
            }
        }
    }
}
