package coms.client.view;

import coms.client.StoreEditDialog;
import coms.client.common.AbstractEditDialog;
import coms.client.common.IEditDialogResultListener;
import coms.client.service.ServicesProvider;
import coms.client.service.StoreDictService;
import coms.client.valueobject.EStore;
import coms.client.valueobject.IValueObject;
import coms.client.view.tablemodel.StoreTableModel;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Stores implements IEditDialogResultListener {
    private StoreDictService storeDictService = ServicesProvider.getInstance().getStoreDictService();
    private JTable table;
    private StoreTablePopUp popUp = new StoreTablePopUp();

    public Stores(JTable table) {
        this.table = table;
        table.setModel(new StoreTableModel());
        table.setAutoCreateRowSorter(true);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    table.setRowSelectionInterval(table.rowAtPoint(e.getPoint()), table.rowAtPoint(e.getPoint()));
                    int index = table.convertRowIndexToModel(table.getSelectedRow());
                    if (index != -1) {
                        StoreTableModel model = (StoreTableModel) table.getModel();
                        popUp.setSelectedStore(model.getStoreAt(index)).show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });
    }

    public void newStore() {
        StoreEditDialog editDialog = new StoreEditDialog(null);
        editDialog.addDialogResultListener(this);
        editDialog.showDialog();
    }

    private void retrieveStores() {
        StoreTableModel model = (StoreTableModel) table.getModel();
        model.setStores(storeDictService.getStoresList());
        model.fireTableDataChanged();
    }

    public void refreshTable() {
        retrieveStores();
        table.getColumn("Склад").setPreferredWidth(3000);
        table.getColumn("Адрес").setPreferredWidth(7000);
    }

    @Override
    public void onEditDialogOK(AbstractEditDialog dialog, IValueObject valueObject) {
        EStore store = (EStore) valueObject;
        if (store.getN() != null)
            storeDictService.editStore(store);
        else
            storeDictService.addStore(store);
        //TODO обновление только скроки, если операция прошла
        refreshTable();
    }

    @Override
    public void onEditDialogCancel(AbstractEditDialog dialog) {}

    private final class StoreTablePopUp extends JPopupMenu {
        private EStore selectedStore;

        public StoreTablePopUp(){
            super();
            JMenuItem item = new JMenuItem("Изменить...");
            item.addActionListener(e -> onEditClick());
            add(item);
            item = new JMenuItem("Удалить");
            item.addActionListener(e -> onRemoveClick());
            add(item);
        }

        public StoreTablePopUp setSelectedStore(EStore selectedStore) {
            this.selectedStore = selectedStore;
            return this;
        }

        private void onEditClick(){
            StoreEditDialog editDialog = new StoreEditDialog(selectedStore);
            editDialog.addDialogResultListener(Stores.this);
            editDialog.showDialog();
        }
        private void onRemoveClick(){
            if (JOptionPane.showConfirmDialog(null, "Вы уверены?\nУдаление склада приведет к удалению всего СТОКА и ДОКУМЕНТОВ данного склада. Эта операция не обратима", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                storeDictService.removeStore(selectedStore);
                refreshTable();
            }
        }
    }
}
