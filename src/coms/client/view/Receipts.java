package coms.client.view;

import coms.client.ReceiptEditDialog;
import coms.client.common.AbstractEditDialog;
import coms.client.common.IEditDialogResultListener;
import coms.client.service.ReceiptService;
import coms.client.service.ServicesProvider;
import coms.client.valueobject.*;
import coms.client.view.tablemodel.ReceiptsTreeTableModel;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;

public class Receipts implements IEditDialogResultListener {
    private ReceiptService receiptService = ServicesProvider.getInstance().getReceiptService();
    private JXTreeTable table;
    private ReceiptPopUpMenu receiptPopUpMenu = new ReceiptPopUpMenu();
    private GoodsPopUpMenu goodsPopUpMenu = new GoodsPopUpMenu();

    public Receipts(JXTreeTable table) {
        this.table = table;
        table.setTreeTableModel(new ReceiptsTreeTableModel(new ArrayList<>(0)));
        table.setAutoCreateRowSorter(true);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (SwingUtilities.isRightMouseButton(e)) {
                    int row = table.rowAtPoint(e.getPoint());
                    if (row == -1)
                        return;
                    table.setRowSelectionInterval(row, row);
                    int index = table.convertRowIndexToModel(table.getSelectedRow());
                    if (index != -1) {
                        TreePath path = table.getPathForRow(index);
                        Object node = path.getLastPathComponent();
                        if (node instanceof EReceipt) {
                            EReceipt receipt = (EReceipt) node;
                            receiptPopUpMenu.setSelectedReceipt(receipt).show(e.getComponent(), e.getX(), e.getY());
                        } else if (node instanceof EReceiptsGoods) {
                            EReceiptsGoods goods = (EReceiptsGoods) node;
                            goodsPopUpMenu.setGoods(goods).show(e.getComponent(), e.getX(), e.getY());
                        }
                    }
                }
            }
        });
    }

    public void newReceipt() {
        ReceiptEditDialog dialog = new ReceiptEditDialog();
        dialog.addDialogResultListener(Receipts.this);
        dialog.showDialog();
    }

    private void retrieveReceipts() {
        table.setTreeTableModel(new ReceiptsTreeTableModel(receiptService.getReceipts()));
        ((AbstractTableModel) table.getModel()).fireTableStructureChanged();
    }

    public void refreshTable() {
        retrieveReceipts();
        table.getColumn("Код").setPreferredWidth(20000);
        table.getColumn("Склад/Товар").setPreferredWidth(40000);
        table.getColumn("Кол-во").setPreferredWidth(5000);
        table.getColumn("Создан").setPreferredWidth(15000);
        table.getColumn("Ожидается").setPreferredWidth(15000);
    }

    @Override
    public void onEditDialogOK(AbstractEditDialog dialog, IValueObject valueObject) {
        EReceipt receipt = (EReceipt) valueObject;
        if (receipt.getN() == null)
            receiptService.addReceipt(receipt);
        refreshTable();
    }

    @Override
    public void onEditDialogCancel(AbstractEditDialog dialog) {

    }

    private class ReceiptPopUpMenu extends JPopupMenu {
        private EReceipt selectedReceipt;

        public ReceiptPopUpMenu() {
            super();
            JMenuItem item = new JMenuItem("Добавить товар...");
            item.addActionListener(e -> onAddClick());
            this.add(item);
            item = new JMenuItem("Принять на склад");
            item.addActionListener(e -> onArrivedClick());
            this.add(item);
            item = new JMenuItem("Удалить");
            item.addActionListener(e -> onDeleteClick());
            this.add(item);
        }

        public ReceiptPopUpMenu setSelectedReceipt(EReceipt selectedReceipt) {
            this.selectedReceipt = selectedReceipt;
            return this;
        }

        private void onAddClick() {
            Object[] variant = ServicesProvider.getInstance().getGoodsDictService().getGoodsList().toArray();
            EGoods result = (EGoods) JOptionPane.showInputDialog(null, "Выберете товар:", "Состав документа", JOptionPane.PLAIN_MESSAGE, null, variant, variant[0]);
            if (result == null) {
                return;
            }
            String code = (String) JOptionPane.showInputDialog(null, "Количество:", "Состав документа", JOptionPane.PLAIN_MESSAGE, null, null, null);
            if (code == null)
                return;
            int amount;
            try {
                amount = Integer.parseInt(code);
            } catch (NumberFormatException e) {
                return;
            }
            if (JOptionPane.showConfirmDialog(null, "Подтвердите добавление:\n" + result + " (" + amount + ")", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                if (selectedReceipt.getReceiptsGoodsesByN() == null)
                    selectedReceipt.setReceiptsGoodsesByN(new ArrayList<>());
                Collection<EReceiptsGoods> goods = selectedReceipt.getReceiptsGoodsesByN();
                EReceiptsGoods rec = new EReceiptsGoods();
                rec.setGoodsByGoodsN(result);
                rec.setAmount(amount);
                rec.setReceiptsByReceiptsN(selectedReceipt);
                goods.add(rec);
                receiptService.editReceipt(selectedReceipt);
                refreshTable();
            }
        }

        private void onArrivedClick() {
            if (selectedReceipt.getReceiptsGoodsesByN().size() > 0) {
                if (JOptionPane.showConfirmDialog(null, "Подтвердите приходование на сток", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    receiptService.toStock(selectedReceipt);
                    refreshTable();
                }
            }
        }

        private void onDeleteClick() {
            if (JOptionPane.showConfirmDialog(null, "Вы уверены, что хотите удалить документ?", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                receiptService.removeReceipt(selectedReceipt);
                refreshTable();
            }
        }
    }

    private class GoodsPopUpMenu extends JPopupMenu {
        private EReceiptsGoods selectedGoods;

        public GoodsPopUpMenu() {
            super();
            JMenuItem item = new JMenuItem("Удалить из документа");
            item.addActionListener(e -> onDeleteClick());
            this.add(item);
        }

        public GoodsPopUpMenu setGoods(EReceiptsGoods goods) {
            this.selectedGoods = goods;
            return this;
        }

        private void onDeleteClick() {
            if (JOptionPane.showConfirmDialog(null, "Вы уверены, что хотите удалить товар?", "Подтверждение", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                EReceipt receipt = selectedGoods.getReceiptsByReceiptsN();
                receipt.getReceiptsGoodsesByN().remove(selectedGoods);
                receiptService.editReceipt(receipt);
                refreshTable();
            }
        }
    }
}
