package coms.client.view.tablemodel;

import coms.client.valueobject.EOrder;
import coms.client.valueobject.EOrdersGoods;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import java.util.Date;
import java.util.List;

public class OrdersTreeTableModel extends AbstractTreeTableModel {
    private static final String[] COLS = {"Код", "Склад/Товар", "Кол-во", "В резерве", "Ожидается выдача"};
    private static final Class[] CLASSES = {String.class, String.class, Integer.class, String.class, Date.class};

    private List<EOrder> orderList;

    public OrdersTreeTableModel(List<EOrder> orderList) {
        super(new Object());
        this.orderList = orderList;
    }

    @Override
    public int getColumnCount() {
        return COLS.length;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return CLASSES[column];
    }

    @Override
    public String getColumnName(int column) {
        return COLS[column];
    }

    @Override
    public Object getValueAt(Object o, int i) {
        if (o instanceof EOrder) {
            EOrder order = (EOrder) o;
            switch (i) {
                case 0:
                    return order.getCode();
                case 1:
                    return order.getStoresByStoresN().getName();
                case 3:
                    return "";
                case 4:
                    return order.getExecuteAt();
                default:
                    return null;
            }
        } else if (o instanceof EOrdersGoods) {
            EOrdersGoods goods = (EOrdersGoods) o;
            switch (i) {
                case 0:
                    return goods.getGoodsByGoodsN().getCode();
                case 1:
                    return goods.getGoodsByGoodsN().getName();
                case 2:
                    return goods.getAmount();
                default:
                    return null;
            }
        }
        return null;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof EOrder) {
            return ((List) ((EOrder) parent).getOrdersGoodsesByN()).get(index);
        } else if (parent instanceof EOrdersGoods)
            return null;
        return orderList.get(index);
    }

    @Override
    public boolean isLeaf(Object node) {
        return node instanceof EOrdersGoods || (node instanceof EOrder && ((EOrder) node).getOrdersGoodsesByN().size() == 0);
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof EOrder)
            return ((EOrder) parent).getOrdersGoodsesByN().size();
        else if (parent instanceof EOrdersGoods)
            return 0;
        return orderList.size();
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent instanceof EOrder) {
            return ((List) ((EOrder) parent).getOrdersGoodsesByN()).indexOf(child);
        } else if (parent instanceof EOrdersGoods)
            return 0;
        return orderList.indexOf(child);
    }
}
