package coms.client.view.tablemodel;

import coms.client.valueobject.EReceipt;
import coms.client.valueobject.EReceiptsGoods;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import java.util.Date;
import java.util.List;

public class ReceiptsTreeTableModel extends AbstractTreeTableModel {
    private static final String[] COLS = {"Код", "Склад/Товар", "Кол-во", "Создан", "Ожидается"};
    private static final Class<?>[] CLASSES = {String.class, String.class, Integer.class, Date.class, Date.class};

    private List<EReceipt> receiptList;

    public ReceiptsTreeTableModel(List<EReceipt> receipts) {
        super(new Object());
        this.receiptList = receipts;
    }

    @Override
    public int getColumnCount() {
        return COLS.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLS[column];
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return CLASSES[column];
    }

    @Override
    public Object getValueAt(Object o, int i) {
        if (o instanceof EReceipt) {
            EReceipt receipt = (EReceipt) o;
            switch (i) {
                case 0:
                    return receipt.getCode();
                case 1:
                    return receipt.getStoresByStoresN().getName();
                case 3:
                    return receipt.getCreatedAt();
                case 4:
                    return receipt.getArrivalAt();
                default:
                    return null;
            }
        } else if (o instanceof EReceiptsGoods) {
            EReceiptsGoods goods = (EReceiptsGoods) o;
            switch (i) {
                case 0:
                    return goods.getGoodsByGoodsN().getCode();
                case 1:
                    return goods.getGoodsByGoodsN().getName();
                case 2:
                    return goods.getAmount();
                default:
                    return null;
            }
        } else
            return null;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof EReceipt) {
            return ((List) ((EReceipt) parent).getReceiptsGoodsesByN()).get(index);
        } else if (parent instanceof EReceiptsGoods) {
            return null;
        }
        return receiptList.get(index);
    }

    @Override
    public boolean isLeaf(Object node) {
        return node instanceof EReceiptsGoods || (node instanceof EReceipt && ((EReceipt) node).getReceiptsGoodsesByN().size() == 0);
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof EReceipt) {
            return ((EReceipt) parent).getReceiptsGoodsesByN().size();
        } else if (parent instanceof EReceiptsGoods) {
            return 0;
        }
        return receiptList.size();
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent instanceof EReceipt) {
            return ((List) ((EReceipt) parent).getReceiptsGoodsesByN()).indexOf(child);
        } else if (parent instanceof EReceiptsGoods)
            return 0;
        return receiptList.indexOf(child);
    }
}
