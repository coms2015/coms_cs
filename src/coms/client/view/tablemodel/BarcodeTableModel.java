package coms.client.view.tablemodel;

import coms.client.valueobject.EGoodsBarcodes;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class BarcodeTableModel extends AbstractTableModel {
    private List<EGoodsBarcodes> barcodes = new ArrayList<>(0);

    public void setBarcodes(List<EGoodsBarcodes> barcodes) {
        this.barcodes = barcodes;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex > 0)
            return null;
        return barcodes.get(rowIndex).getBarcode();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public String getColumnName(int column) {
        return column == 0 ? "Штрихкод" : null;
    }

    @Override
    public int getRowCount() {
        return barcodes.size();
    }

    public EGoodsBarcodes getBarcodeAt(int rowIndex) {
        return barcodes.get(rowIndex);
    }
}
