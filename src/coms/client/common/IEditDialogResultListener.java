package coms.client.common;

import coms.client.valueobject.IValueObject;

public interface IEditDialogResultListener {
    void onEditDialogOK(AbstractEditDialog dialog, IValueObject valueObject);
    void onEditDialogCancel(AbstractEditDialog dialog);
}