package coms.client.common;

import coms.client.valueobject.IValueObject;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractEditDialog extends JDialog {
    private Set<IEditDialogResultListener> listeners = new HashSet<>();
    protected IValueObject data;

    public void addDialogResultListener(IEditDialogResultListener listener) {
        listeners.add(listener);
    }

    public void removeDialogResultListener(IEditDialogResultListener listener) {
        listeners.remove(listener);
    }

    protected void callbackOK() {
        for (IEditDialogResultListener listener : listeners)
            listener.onEditDialogOK(this, data);
    }

    protected void callbackCansel() {
        for (IEditDialogResultListener listener : listeners)
            listener.onEditDialogCancel(this);
    }

    public void showDialog() {
        this.pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
