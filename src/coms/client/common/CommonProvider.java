package coms.client.common;

import com.sun.istack.internal.Nullable;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import coms.client.Settings;
import coms.client.exception.*;
import coms.client.service.AuthService;
import coms.client.service.ServicesProvider;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.NewCookie;
import java.io.IOException;
import java.util.List;

public class CommonProvider<To, From> {
    private final Class<From> fromClass;

    private AuthService authService = ServicesProvider.getInstance().getAuthService();

    private ObjectMapper mapper = new ObjectMapper();
    private WebResource resource;

    public CommonProvider(String path, Class<From> fromClass) {
        resource = Client.create().resource(Settings.url + path);
        this.fromClass = fromClass;
    }

    public List<From> get() throws AuthException, PrivilegesException, ServerException, WrongResponseException, WrongRequestException {

        ClientResponse response = resource.accept("application/json").cookie(new NewCookie("JSESSIONID", authService.getSessionId())).get(ClientResponse.class);
        authCookies(response.getCookies());
        if (response.getStatus() == 401) // 401 Unauthorized
            throw new AuthException();
        if (response.getStatus() == 403) // 403 Forbidden
            throw new PrivilegesException();
        if (response.getStatus() == 500) // 500 Internal Server Error
            throw new ServerException();
        if (response.getStatus() == 400) // 500 Internal Server Error
            throw new WrongRequestException();
        if (response.getStatus() != 200) // 200 OK
            throw new ServerException("http get failed. Code: " + response.getStatus());
        try {
            return mapper.readValue(response.getEntity(String.class), mapper.getTypeFactory().constructCollectionType(List.class, fromClass));
        } catch (JsonMappingException | JsonParseException e) {
            throw new WrongResponseException(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public From post(To request) throws AuthException, PrivilegesException, ServerException, WrongResponseException, WrongRequestException {
        String strRequest;
        try {
            strRequest = mapper.writeValueAsString(request);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ClientResponse response = resource.type("application/json").cookie(new NewCookie("JSESSIONID", authService.getSessionId())).post(ClientResponse.class, strRequest);
        authCookies(response.getCookies());
        if (response.getStatus() == 401) // 401 Unauthorized
            throw new AuthException();
        if (response.getStatus() == 403) // 403 Forbidden
            throw new PrivilegesException();
        if (response.getStatus() == 500) // 500 Internal Server Error
            throw new ServerException();
        if (response.getStatus() == 400) // 500 Internal Server Error
            throw new WrongRequestException();
        if (response.getStatus() != 200) // 200 OK
            throw new ServerException("http post failed. Code: " + response.getStatus());
        try {
            return mapper.readValue(response.getEntity(String.class), fromClass);
        } catch (JsonMappingException | JsonParseException e) {
            throw new WrongResponseException(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void authCookies(List<NewCookie> cookies) {
        cookies.stream().filter(cookie -> cookie.getName().equals("JSESSIONID")).forEach(cookie -> authService.setSessionId(cookie.getValue()));
    }
}
