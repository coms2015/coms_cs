package coms.exchange;

import coms.client.valueobject.EOrder;

public class JOrderAdd {
    private Integer addOrder;
    private EOrder order;

    public Integer getAddOrder() {
        return addOrder;
    }

    public void setAddOrder(Integer addOrder) {
        this.addOrder = addOrder;
    }

    public EOrder getOrder() {
        return order;
    }

    public void setOrder(EOrder order) {
        this.order = order;
    }
}
