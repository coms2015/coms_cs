package coms.exchange;

import coms.client.valueobject.EOrder;

public class JOrderOut {
    private Integer outOrder;
    private EOrder order;

    public Integer getOutOrder() {
        return outOrder;
    }

    public void setOutOrder(Integer outOrder) {
        this.outOrder = outOrder;
    }

    public EOrder getOrder() {
        return order;
    }

    public void setOrder(EOrder order) {
        this.order = order;
    }
}
