package coms.exchange;

import coms.client.valueobject.EStore;

public class JStoreAdd {
    private Integer addStore;
    private EStore store;

    public Integer getAddStore() {
        return addStore;
    }

    public void setAddStore(Integer addStore) {
        this.addStore = addStore;
    }

    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }
}
