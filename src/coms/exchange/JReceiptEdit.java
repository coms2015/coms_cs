package coms.exchange;

import coms.client.valueobject.EReceipt;

public class JReceiptEdit {
    private Integer editReceipt;
    private EReceipt receipt;

    public Integer getEditReceipt() {
        return editReceipt;
    }

    public void setEditReceipt(Integer editReceipt) {
        this.editReceipt = editReceipt;
    }

    public EReceipt getReceipt() {
        return receipt;
    }

    public void setReceipt(EReceipt receipt) {
        this.receipt = receipt;
    }
}
