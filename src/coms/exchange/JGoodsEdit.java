package coms.exchange;

import coms.client.valueobject.EGoods;

public class JGoodsEdit {
    private Integer editGoods;
    private EGoods goods;

    public Integer getEditGoods() {
        return editGoods;
    }

    public void setEditGoods(Integer editGoods) {
        this.editGoods = editGoods;
    }

    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }
}