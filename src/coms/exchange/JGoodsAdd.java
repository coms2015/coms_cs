package coms.exchange;

import coms.client.valueobject.EGoods;

public class JGoodsAdd {
    private Integer addGoods;
    private EGoods goods;

    public Integer getAddGoods() {
        return addGoods;
    }

    public void setAddGoods(Integer addGoods) {
        this.addGoods = addGoods;
    }

    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }
}
